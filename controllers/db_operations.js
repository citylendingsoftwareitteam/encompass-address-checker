const AWS = require("aws-sdk");
const DYNAMO_DB = new AWS.DynamoDB.DocumentClient({region: 'us-east-1', endpoint: 'http://localhost:8000'});

module.exports.put = async (table_name, db_item) => {
  let params = {
    TableName: table_name,
    Item: db_item
  };

  DYNAMO_DB.put(params, (error) => {
    if (error) {
      console.error(error);
      return {
        statusCode: error.statusCode || 501,
        headers: {
          'Content-Type': 'application/json'
        },
        body: 'Could not create the item'
      };
    }
    console.log('hola')
    return {
      statusCode: 200,
      body: JSON.stringify(params.Item)
    };
  });
}

module.exports.get = async (table_name, db_key) => {
  let params = {
    TableName: table_name,
    Key: {
      loan_guid: db_key
    }
  };

  try {
    return await new Promise((resolve, reject) => {
      DYNAMO_DB.get(params, (error, result) => {
        if (error) {
          console.error(error)
          reject({
            status: error.statusCode || 501,
            headers: {
              'Content-Type': 'application/json'
            }
          });
        }
  
        resolve({
          status: 200,
          body: result.Item
        });
      });
    });
  } catch (error) {
    console.error(error);
    return {
      status: 500,
      body: 'error on getting a item'
    };
  }
}

module.exports.update = async (table_name, db_key, new_value) => {
  const timestamp = new Date().getTime();
  const params = {
    TableName: table_name,
    Key: {
      loan_guid: db_key
    },
    ExpressionAttributeValues: {
      ':address_validated': new_value,
      ':updatedAt': timestamp
    },
    UpdateExpression: 'SET address_validated = :address_validated, updatedAt = :updatedAt',
    ReturnValues: 'ALL_NEW'
  };

  try {
    return new Promise((resolve, reject) => {
      DYNAMO_DB.update(params, (error, result) => {
        if (error) {
          console.error(error);
          reject({
            status: error.statusCode || 501,
            headers: {
              'Content-Type': 'application/json'
            }
          });
        }
        resolve({
          status: 200,
          body: result.Attributes
        });
      })
    });
  } catch (error) {
    console.error(error);
    return {
      status: 500,
      body: 'error on updating a item'
    };
  }
}