const node_mailer = require("nodemailer");

const FROM_EMAIL = 'Encompass_alerts@citylendinginc.com';
const FROM_PASSWORD = 'Melkas13';
const TRANSPORTER = node_mailer.createTransport({
  host: 'smtp.office365.com',
  secure: false,
  port: 587,
  auth: {
    user: FROM_EMAIL,
    pass: FROM_PASSWORD
  },
  tls: {
    ciphers: 'SSLv3'
  }
});

module.exports.sendEmail = (to_email, subject_email, body_email) => {
  let mail_options = {
    from: FROM_EMAIL,
    to: to_email,
    cc: 'alopez@citylendinginc.com',
    subject: subject_email,
    text: body_email
  };

  TRANSPORTER.sendMail(mail_options, (error, info) => {
    if (error) {
      console.error(error);
    }
  })
}

module.exports.sendMassiveEmails = (emails_to_send, subject_email, body_email) => {
  for(let email in emails_to_send) {
    let mail_options = {
      from: FROM_EMAIL,
      to: email,
      subject: subject_email,
      text: body_email
    };
  
    TRANSPORTER.sendMail(mail_options, (error, info) => {
      if (error) {
        console.error(error);
      }
    })
  }
}