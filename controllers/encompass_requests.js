'use strict';

const axios = require("axios");
const querystring = require("querystring");
const xml_convert = require("xml-js");

module.exports.userToken = async (username, password) => {
    try {
        const response = await axios({
            method: 'post',
            url: 'https://api.elliemae.com/oauth2/v1/token',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            data: querystring.stringify({
                grant_type: 'password',
                username: `${username}@encompass:BE11139022`,
                password: password,
                client_id: 'wpdefk4',
                client_secret: 'puH0BDobesM!TwP^HtwTrFm^7&FoB6s4KKtjWUrm*hui7U^UNy1sY4MDJigsVdRv'
            })
        });
        return response.data.access_token;
    } catch (error) {
        console.error(error);
    }
}

module.exports.isValidated = async (access_token, loan_guid) => {
  try {
    const request = await axios({
      method: 'POST',
      url: `https://api.elliemae.com/encompass/v1/loans/${loan_guid}/fieldReader`,
      data: [
        "CX.USPSVALIDATION",
        "364",
        "11",
        "12",
        "14",
        "15",
        "LoanTeamMember.Email.Loan Processor",
        "LoanTeamMember.Email.Loan Officer",
        "LoanTeamMember.Email.LO Assistant"
      ],
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${access_token}`
      }
    });

    const response = request.data;
    const validated = (response[0].hasOwnProperty('value') && (response[0]['value'] != "" && response[0]['value'] == 'Y'));

    return {
      error: false,
      loan_number: response[1]['value'],
      address_is_validated: validated,
      address_info: {
        street: response[2]['value'],
        city: response[3]['value'],
        state: response[4]['value'],
      },
      emails: {
        lo_proccessor: response[5]['value'],
        lo: response[6]['value'],
        lo_assistant: response[7]['value']
      }
    };
  } catch (error) {
    console.error(error);
  }
}

module.exports.validateAddress = async (access_token, loan_guid) => {
  try {
    const request = await axios({
      method: 'POST',
      url: 'https://api.elliemae.com/encompass/v1/loanPipeline',
      params: {
        limits: 2000
      },
      data: {
        loanGuids: [
          loan_guid
        ],
        fields: [
          "Fields.11",
          "Fields.12",
          "Fields.14",
          "Fields.15"
        ]
      },
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${access_token}`
      }
    });
    const response = request.data;

    if (response !== undefined && response.length > 0 ) {
      const usps_response = this.checkFromUSPS(response[0]['fields']);

      return usps_response;
    } else {
      return {
        error: true,
        description: 'loan not found'
      };
    }
  } catch (error) {
    console.error(error);
    return {
      error: true,
      message: 'An error occurred on validating address',
      status: 500
    };
  }
}

module.exports.checkFromUSPS = async (address_obj) => {
  let address = address_obj['Fields.11'];
  let city = address_obj['Fields'];
  let state = address_obj['Fields.14'];
  let zip_5 = address_obj['Fields.15'];

  try {
    const request = await axios({
      method: 'POST',
      url: 'https://secure.shippingapis.com/ShippingAPI.dll',
      params: {
        API: 'Verify',
        XML: `<AddressValidateRequest USERID="015CITYL7013">
                <Revision>1</Revision>
                <Address ID="0">
                    <Address1/>
                    <Address2>${address}</Address2>
                    <City>${city}</City>
                    <State>${state}</State>
                    <Zip5>${zip_5}</Zip5>
                    <Zip4/>
                </Address>
              </AddressValidateRequest>`
      }
    });

    const xml_response = request.data;
    const json_obj = JSON.parse(xml_convert.xml2json(xml_response, {compact: true}));
    const address_usps = json_obj['AddressValidateResponse']['Address'];
    if (address_usps.hasOwnProperty('Error')) {
      return {
        error: true,
        number: address_usps['Error']['Number']['_text'],
        description: address_usps['Error']['Description']['_text']
      };
    } else {
      return {
        error: false,
        address: address_usps['Address2']['_text'],
        city: address_usps['City']['_text'],
        state: address_usps['State']['_text'],
        zip: address_usps['Zip5']['_text']
      };
    }
  } catch (error) {
    console.error(error);
    return {
      error: true,
      message: 'An error occurred on checking address in USPS',
      status: 500
    };
  }
}

module.exports.changeAddress = async (access_token, loan_guid, property_info) => {
  try {
    const request = await axios({
      method: 'PATCH',
      url: `https://api.elliemae.com/encompass/v1/loans/${loan_guid}`,
      params: {
        view: 'id'
      },
      data: {
        property: {
          streetAddress: property_info['street'],
          city: property_info['city'],
          postalCode: property_info['zip'],
          state: property_info['state']
        },
        customFields: [{
          id: 'CX.USPSVALIDATION',
          stringValue: 'Y'
        }]
      },
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${access_token}`
      }
    });

    const response = request.data
    if (request.status == 200 || request.status == 201) {
      return {
        error: false,
        message: 'address was changed successfully',
        loan_guid,
        status: 200,
        encompass_response: response
      };
    } else {
      return {
        error: true,
        message: 'An error occurred on changing address',
        status: 500,
        encompass_response: response
      };
    }

  } catch (error) {
    console.error(error);
    return {
      error: true,
      message: 'An error occurred on changing address',
      status: 500,
      encompass_response: response
    };
  }
}

module.exports.revokeToken = async (accessToken) => {
    try {
        const response = await axios({
            method: 'post',
            url: 'https://api.elliemae.com/oauth2/v1/token/revocation',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            data: querystring.stringify({ token: accessToken }),
            auth: {
              username: 'wpdefk4',
              password: 'puH0BDobesM!TwP^HtwTrFm^7&FoB6s4KKtjWUrm*hui7U^UNy1sY4MDJigsVdRv',
            }
        });
        
        return response.data;
    } catch (error) {
        console.log(error);
    }
}

/*
  Sample input:
  <AddressValidateRequest USERID="015CITYL7013">
    <Revision>1</Revision>
    <Address ID="0">
        <Address1/>
        <Address2>8150 Leesburg Pike, 405</Address2>
        <City>Vienna</City>
        <State>VA</State>
        <Zip5>22182</Zip5>
        <Zip4/>
    </Address>
  </AddressValidateRequest>
*/