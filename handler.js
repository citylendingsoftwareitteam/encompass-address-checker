'use strict';

const ENCOMPASS = require("./controllers/encompass_requests");
const MAILER = require("./controllers/mailer");
const DYNAMO_DB = require("./controllers/db_operations");

const TABLE_NAME = 'Encompass_Address_Recomentation';
const USERNAME = 'admin';
const PASSWORD = 'L3nd1ng';

module.exports.checkAddress = async event => {
  const loan_guid = '{6d7c6426-9762-455f-8393-3084cb191e91}';
  let access_token = await ENCOMPASS.userToken(USERNAME, PASSWORD);
  let check_response = {};

  const is_validated = await ENCOMPASS.isValidated(access_token, loan_guid);

  const loan_number = is_validated['loan_number'];
  const current_street = is_validated['address_info']['street'];
  const current_city = is_validated['address_info']['city'];
  const current_state = is_validated['address_info']['state'];
  const emails_to_send = [
    is_validated['emails']['lo'],
    is_validated['emails']['lo_assistant'],
    is_validated['emails']['lo_proccessor']
  ]
  if (!is_validated['address_is_validated']) {
    check_response = await ENCOMPASS.validateAddress(access_token, loan_guid);
    if (!check_response['error']) {
      MAILER.sendEmail(emails_to_send, `Address recomendation for Loan ${loan_number}`, `The current address is:\nStreet: ${current_street}\nCity: ${current_city}\nState: ${current_state}\n\nThe address recommended by USPS is:\nStreet: ${check_response['address']}\nCity: ${check_response['city']}\nState: ${check_response['state']}`);
      DYNAMO_DB.put(TABLE_NAME, {
        loan_guid,
        street: check_response['address'],
        city: check_response['city'],
        state: check_response['state'],
        zip: check_response['zip'],
        address_validated: 'N'
      });
    } else {
      MAILER.sendEmail(emails_to_send, `Error on submitted address for Loan ${loan_number}`, `An error has raised when the current address was being validated.\nError number: ${check_response['number']}\nError description: ${check_response['description']}`)
    }
  } else {
    check_response = {
      error: false,
      message: 'address validated'
    }
  }
  ENCOMPASS.revokeToken(access_token);

  return {
    status: 200,
    body: JSON.stringify({
      check_response
    })
  };
}

module.exports.changeAddress = async event => {
  const loan_guid = '{6d7c6426-9762-455f-8393-3084cb191e91}';
  const db_response = await DYNAMO_DB.get(TABLE_NAME, loan_guid);

  if (db_response['body'] === undefined) {
    return {
      error: true,
      status: 400,
      body: JSON.stringify({
        message: 'property not found'
      })
    };
  }

  if (db_response['body']['address_validated'] == 'Y') {
    return {
      status: 200,
      body: JSON.stringify({
        error: false,
        message: 'loan has already updated'
      })
    };
  }

  let access_token = await ENCOMPASS.userToken(USERNAME, PASSWORD);
  const response = await ENCOMPASS.changeAddress(access_token, loan_guid, db_response['body']);
  await DYNAMO_DB.update(TABLE_NAME, loan_guid, 'Y');

  ENCOMPASS.revokeToken(access_token);

  return {
    status: 200,
    body: JSON.stringify({
      response
    })
  };
}